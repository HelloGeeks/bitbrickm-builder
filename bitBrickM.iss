; Script generated by the Inno Setup Script Wizard.
; SEE THE DOCUMENTATION FOR DETAILS ON CREATING INNO SETUP SCRIPT FILES!

#define MyAppName "bitBrickM"
#define MyAppVersion "1.0.0"
#define MyAppPublisher "Hellogeeks"
#define MyAppURL "http://bitbrick.cc"
#define MyAppExeName "bitBrickM.exe"

[Setup]
; NOTE: The value of AppId uniquely identifies this application.
; Do not use the same AppId value in installers for other applications.
; (To generate a new GUID, click Tools | Generate GUID inside the IDE.)
AppId={{A2BF9D22-1179-47A2-8462-C173B27D7E83}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}
AppUpdatesURL={#MyAppURL}
DefaultDirName={pf}\{#MyAppName}
DisableProgramGroupPage=yes
OutputBaseFilename=setup
Compression=lzma
SolidCompression=yes
SetupIconFile=.\icon.ico
UninstallDisplayIcon=.\icon.ico 


[Languages]
Name: "kr"; MessagesFile: ".\4_InstallerResources\lang\Korean.isl"
Name: "en"; MessagesFile: ".\4_InstallerResources\lang\English.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}";

[Files]
Source: ".\Driver\mbedWinSerial_16466.exe"; DestDir: "{app}\Driver"; DestName: mbedWinSerial_16466.exe; Flags: ignoreversion
Source: ".\bitBrickM\bitBrickM.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: ".\bitBrickM\Microbit\bitBrickM_firmware.hex"; DestDir: "{%USERPROFILE}\Documents\bitBrickM"; Flags: ignoreversion
Source: ".\bitBrickM\bitBrickM_Example\*"; DestDir: "{%USERPROFILE}\Documents\bitBrickM"; Flags: ignoreversion
Source: ".\bitBrickM\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs
; NOTE: Don't use "Flags: ignoreversion" on any shared system files

[Icons]
Name: "{commonprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent
Filename: "{app}\Driver\mbedWinSerial_16466.exe"; Flags: runascurrentuser runhidden waituntilterminated; StatusMsg: "Installing mbedWinSerial Driver..."
